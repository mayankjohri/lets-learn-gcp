terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "3.45.0"
    }
  }
}

provider "google" {
  credentials = file("~/.gcp/solar-galaxy.json")

  project = "solar-galaxy-305509"
  region  = "us-central1"
  zone    = "us-central1-c"
}

resource "google_compute_network" "vpc_network" {

  name = "terraform-networking"
  mtu = 1400

}	

resource "google_compute_address" "vm_static_ip" {
  name = "terraform-static-ip"
}
