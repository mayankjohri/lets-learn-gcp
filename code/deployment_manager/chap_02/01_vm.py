# from constants import COMPUTE_URL_BASE
# from proj_vals import MY_PROJECT, ZONE, MACHINE_TYPE

COMPUTE_URL_BASE = 'https://www.googleapis.com/compute/v1/'


def generate_config(context):
  """Creates the first virtual machine."""
  zone = context.properties['zone']
  project = context.properties['MY_PROJECT']
  machineType = context.properties['machineType']
  resources = [{
      'name': 'python-vm',
      'type': 'compute.v1.instance',
      'properties': {
          'zone': zone,
          'machineType': ''.join([COMPUTE_URL_BASE,
                                  f'projects/' + project,
                                  '/zones/' + zone + '/',
                                  'machineTypes/' + machineType]),
          'disks': [{
              'deviceName': 'boot',
              'type': 'PERSISTENT',
              'boot': True,
              'autoDelete': True,
              'initializeParams': {
                  'sourceImage': ''.join([COMPUTE_URL_BASE, 'projects/',
                                          'debian-cloud/global/',
                                          'images/family/debian-9'])
              }
          }],
          'networkInterfaces': [{
              'network': '$(ref.python-network.selfLink)',
              'accessConfigs': [{
                  'name': 'External NAT',
                  'type': 'ONE_TO_ONE_NAT'
              }]
          }]
      }
  }]
  return {'resources': resources}
