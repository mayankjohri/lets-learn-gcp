COMPUTE_URL_BASE = 'https://www.googleapis.com/compute/v1/'


def generate_config(context):
  """Creates the first virtual machine."""
  print(dir(context))
  name = context.env['name']

  resources = [{
      'name': name,
      'type': 'compute.v1.network',
      'properties': {
          'autoCreateSubnetworks': False

      }
  }]
  return {'resources': resources}
