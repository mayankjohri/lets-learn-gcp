COMPUTE_URL_BASE = 'https://www.googleapis.com/compute/v1/'


def generate_config(context):
  """Creates the first virtual machine."""
  print(dir(context))
  name = context.env['name']
  # name = "test"
  network = context.properties.get('network')
  ip_protocol = context.properties.get('IPProtocol')
  resources = [{
      'name': name + "-allow-http",
      'type': 'compute.v1.firewall',
      'properties': {
          'network': network,
          'sourceRanges': ["0.0.0.0/0"],
          'allowed': [{
              'IPProtocol': ip_protocol,
              'ports': ["80"]
          }]
      }
  }]
  return {'resources': resources}
