COMPUTE_URL_BASE = 'https://www.googleapis.com/compute/v1/'


def generate_config(context):
  """Creates the first virtual machine."""
  print(dir(context))
  name = context.env['name']
  ip_cidr_range = context.properties.get('ipCidrRange')
  network = context.properties.get('network')
  region = context.properties.get('region')
  resources = [{
      'name': name,
      'type': 'compute.v1.subnetwork',
      'properties': {
          'ipCidrRange': ip_cidr_range,
          'network': network,
          'region': region
      }
  }]
  return {'resources': resources}
