# Vim Cheat Sheet

## Cursor movement (Inside command/normal mode)

![The four directions in Vim, keys h, j, k, and l.](http://vimsheet.com/images/hjkl.png)

- `w` - jump by start of words (punctuation considered words)
- `W` - jump by words (spaces separate words)
- `e` - jump to end of words (punctuation considered words)
- `E` - jump to end of words (no punctuation)
- `b` - jump backward by words (punctuation considered words)
- `B` - jump backward by words (no punctuation)
- `0` - (zero) start of line
- `^` - first non-blank character of line (same as 0w)
- `$` - end of line
- Advanced (in order of what I find most useful)
  - `Ctrl+d` - move down half a page
  - `Ctrl+u` - move up half a page
  - `}` - go forward by paragraph (the next blank line)
  - `{` - go backward by paragraph (the next blank line)
  - `gg` - go to the top of the page
  - `G` - go the bottom of the page
  - `: [num] [enter]` - Go to that line in the document
  - Searching
    - `f [char]` - Move to the next char on the current line after the cursor
    - `F [char]` - Move to the next char on the current line before the cursor
    - `t [char]` - Move to before the next char on the current line after the cursor
    - `T [char]` - Move to before the next char on the current line before the cursor
    - All these commands can be followed by `;` (semicolon) to go to the next searched item, and `,` (comma) to go the previous searched item

## Insert/Appending/Editing Text

- Results in Insert mode

  - `i` - start insert mode at cursor
  - `I` - insert at the beginning of the line
  - `a` - append after the cursor
  - `A` - append at the end of the line
  - `o` - open (append) blank line below current line (no need to press return)
  - `O` - open blank line above current line
  - `cc` - change (replace) an entire line
  - `c [movement command]` - change (replace) from the cursor to the move-to point.
  - ex. `ce` changes from the cursor to the end of the cursor word

- `Esc` or `Ctrl+[` - exit insert mode

- `r [char]` - replace a single character with the specified char (does not use Insert mode)

- ```plaintext
  d
  ```

  \- delete

  - `d` - [movement command] deletes from the cursor to the move-to point.
  - ex. `de` deletes from the cursor to the end of the current word

- `dd` - delete the current line

- Advanced

  - `J` - join line below to the current one

## Marking text (visual mode)

- ```plaintext
  v
  ```

  \- starts visual mode

  - From here you can move around as in normal mode (`h`, `j`, `k`, `l` etc.) and can then do a command (such as `y`, `d`, or `c`)

- `V` - starts linewise visual mode

- `Ctrl+v` - start visual block mode

- `Esc` or `Ctrl+[` - exit visual mode

- Advanced

  - `O` - move to other corner of block
  - `o` - move to other end of marked area

## Visual commands

Type any of these while some text is selected to apply the action

- `y` - yank (copy) marked text
- `d` - delete marked text
- `c` - delete the marked text and go into insert mode (like c does above)

## Cut and Paste

- `yy` - yank (copy) a line
- `p` - put (paste) the clipboard after cursor
- `P` - put (paste) before cursor
- `dd` - delete (cut) a line
- `x` - delete (cut) current character
- `X` - delete previous character (like backspace)

## Exiting

- `:w` - write (save) the file, but don’t exit
- `:wq` - write (save) and quit
- `:q` - quit (fails if anything has changed)
- `:q!` - quit and throw away changes

## Search/Replace

- `/pattern` - search for pattern
- `?pattern` - search backward for pattern
- `n` - repeat search in same direction
- `N` - repeat search in opposite direction
- `:%s/old/new/g` - replace all old with new throughout file ([gn](http://vimcasts.org/episodes/operating-on-search-matches-using-gn/) is better though)
- `:%s/old/new/gc` - replace all old with new throughout file with confirmations

## Working with multiple files

- `:e filename` - Edit a file
- `:tabe` - Make a new tab
- `gt` - Go to the next tab
- `gT` - Go to the previous tab
- Advanced
  - `:vsp` - vertically split windows
  - `ctrl+ws` - Split windows horizontally
  - `ctrl+wv` - Split windows vertically
  - `ctrl+ww` - switch between windows
  - `ctrl+wq` - Quit a window

## Marks

Marks allow you to jump to designated points in your code.

- `m{a-z}` - Set mark {a-z} at cursor position
- A capital mark {A-Z} sets a global mark and will work between files
- `‘{a-z}` - move the cursor to the start of the line where the mark was set
- `‘’` - go back to the previous jump location

## General

- `u` - undo
- `Ctrl+r` - redo
- `.` - repeat last command

## .vimrc

 You might wish to install a vim plugin manager such as `vim-plug` by running the following command 

```bash
$:> curl -fLo ~/.vim/autoload/plug.vim --create-dirs     https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```

It allows you to install all the third party plugins available for vim.

You might wish to customise vim using `.vimrc` file. Sample file is as follows

```
set nocompatible
syntax on
filetype plugin indent on
set encoding=UTF-8

" Specify a directory for plugins
" - For Neovim: stdpath('data') . '/plugged'
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.vim/plugged')

" Make sure you use single quotes
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'ryanoasis/vim-devicons'

" Shorthand notation; fetches https://github.com/junegunn/vim-easy-align
Plug 'junegunn/vim-easy-align'

" Any valid git URL is allowed
Plug 'https://github.com/junegunn/vim-github-dashboard.git'

" Multiple Plug commands can be written in a single line using | separators
Plug 'SirVer/ultisnips' | Plug 'honza/vim-snippets'

" On-demand loading
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'tpope/vim-fireplace', { 'for': 'clojure' }

" Using a non-default branch
" Plug 'rdnetto/YCM-Generator', { 'branch': 'stable' }

Plug 'Shougo/deoplete.nvim'
Plug 'roxma/nvim-yarp'
Plug 'roxma/vim-hug-neovim-rpc'

" Using a tagged release; wildcard allowed (requires git 1.9.2 or above)
Plug 'fatih/vim-go', { 'tag': '*' }

" Plugin options
Plug 'nsf/gocode'
" Plugin outside ~/.vim/plugged with post-update hook
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }

Plug 'hashivim/vim-terraform'
Plug 'vim-syntastic/syntastic'
Plug 'juliosueiras/vim-terraform-completion'

" Initialize plugin system
call plug#end()

let g:deoplete#enable_at_startup = 1

au! BufNewFile,BufRead terragrunt.hcl set filetype=terraform syntax=terraform
let g:terraform_align=1
let g:terraform_fmt_on_save=1

" Syntastic Config
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

" (Optional)Remove Info(Preview) window
set completeopt-=preview

" (Optional)Hide Info(Preview) window after completions
autocmd CursorMovedI * if pumvisible() == 0|pclose|endif
autocmd InsertLeave * if pumvisible() == 0|pclose|endif

" (Optional) Enable terraform plan to be include in filter
let g:syntastic_terraform_tffilter_plan = 1

" (Optional) Default: 0, enable(1)/disable(0) plugin's keymapping
let g:terraform_completion_keys = 1

" (Optional) Default: 1, enable(1)/disable(0) terraform module registry completion
let g:terraform_registry_module_completion = 0

let g:airline_theme='simple'
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '|'
let g:airline#extensions#tabline#formatter = 'default'
```

and  run the following in vim.

```
:source %
:PlugInstall
```

## References

- https://www.redhat.com/sysadmin/five-vim-plugins
- https://vimawesome.com/
- https://vimawesome.com/plugin/vim-terraform-state-of-grace
- http://hashivim.github.io/
- https://vimawesome.com/plugin/vim-terraform-completion#vim-plug
- https://github.com/Shougo/deoplete.nvim