# App Dev - Deploying the Application into App Engine Flexible Environment: Python

```python
for a in range(10):
    print(a)
else:
    print(a)
```

## Overview

An App Engine app is a single application resource with one or more services. Each service can be configured to use different runtimes and to operate with different performance settings. Within each service, you can deploy versions of that service, and each then runs within one or more instances, depending on how much traffic you configured it to handle. For more information, see [An Overview of App Engine](https://cloud.google.com/appengine/docs/flexible/python/an-overview-of-app-engine).

App Engine uses either a Standard or Flexible environment. A standard environment runs instances in a sandbox, limiting available CPU options and disc access.

In contrast, a flexible environment runs your application in Docker containers on Google Compute Engine virtual machines (VMs), which have fewer restrictions. For example, you can use the programming language or library of your choice, write to disk, and even run multiple processes. You also have the choice of Compute Engine machine types for your instances. For more information about App Engine environments, see [The App Engine Standard Environment](https://cloud.google.com/appengine/docs/standard/) and [App Engine Flexible Environment](https://cloud.google.com/appengine/docs/flexible/).

In this lab, you deploy the Quiz application into App Engine flexible environment, leveraging App Engine features, including versions and traffic splitting.

## Objectives

In this lab, you learn how to perform the following tasks:

- Create an app.yaml file to describe the App Engine flexible environment requirements for an application.
- Deploy the quiz application into App Engine flexible environment.
- Employ versions and traffic splitting to perform A/B testing of an application feature.

## Setup and requirements

For each lab, you get a new GCP project and set of resources for a fixed time at no cost.

1. Make sure you signed into Qwiklabs using an **incognito window**.
2. Note the lab's access time (for example, ![img/time.png](https://cdn.qwiklabs.com/aZQJ4BT7uCmM9XR6BTXgTRP1Hfu1T7q6V%2BcnbdEsbpU%3D) and make sure you can finish in that time block.

There is no pause feature. You can restart if needed, but you have to start at the beginning.

1. When ready, click ![img/start_lab.png](https://cdn.qwiklabs.com/XE8x7uvQokyubNwnYKKc%2BvBBNrMlo5iNZiDDzQQ3Ddo%3D).
2. Note your lab credentials. You will use them to sign in to Cloud Platform Console. ![img/open_google_console.png](https://cdn.qwiklabs.com/0d78dhX6IVMVWmixCPPSBbmi5O2GPokCXf1Ps1AkTgI%3D)
3. Click **Open Google Console**.
4. Click **Use another account** and copy/paste credentials for **this** lab into the prompts.

If you use other credentials, you'll get errors or **incur charges**.

1. Accept the terms and skip the recovery resource page.

Do not click **End Lab** unless you are finished with the lab or want to restart it. This clears your work and removes the project.

### Activate Google Cloud Shell

Google Cloud Shell is a virtual machine that is loaded with development tools. It offers a persistent 5GB home directory and runs on the Google Cloud. Google Cloud Shell provides command-line access to your GCP resources.

1. In GCP console, on the top right toolbar, click the Open Cloud Shell button.

   ![Cloud Shell icon](https://cdn.qwiklabs.com/vdY5e%2Fan9ZGXw5a%2FZMb1agpXhRGozsOadHURcR8thAQ%3D)

2. Click **Continue**. ![cloudshell_continue.png](https://cdn.qwiklabs.com/lr3PBRjWIrJ%2BMQnE8kCkOnRQQVgJnWSg4UWk16f0s%2FA%3D)

It takes a few moments to provision and connect to the environment. When you are connected, you are already authenticated, and the project is set to your *PROJECT_ID*. For example:

![Cloud Shell Terminal](https://cdn.qwiklabs.com/hmMK0W41Txk%2B20bQyuDP9g60vCdBajIS%2B52iI2f4bYk%3D)

**gcloud** is the command-line tool for Google Cloud Platform. It comes pre-installed on Cloud Shell and supports tab-completion.

You can list the active account name with this command:

```
gcloud auth list
content_copy
```

Output:

```output
Credentialed accounts:
 - <myaccount>@<mydomain>.com (active)content_copy
```

Example output:

```Output
Credentialed accounts:
 - google1623327_student@qwiklabs.netcontent_copy
```

You can list the project ID with this command:

```
gcloud config list project
content_copy
```

Output:

```output
[core]
project = <project_ID>content_copy
```

Example output:

```Output
[core]
project = qwiklabs-gcp-44776a13dea667a6content_copy
```

Full documentation of **gcloud** is available on [Google Cloud gcloud Overview ](https://cloud.google.com/sdk/gcloud).

## Prepare the case study application

In this section, you access Cloud Shell, clone the git repository containing the Quiz application, configure environment variables, and run the application.

### Clone source code in Cloud Shell

1. Clone the repository for the class.

```bash
git clone https://github.com/GoogleCloudPlatform/training-data-analystcontent_copy
```

1. Create a soft link as a shortcut to your working directory:

```bash
ln -s ~/training-data-analyst/courses/developingapps/v1.2/python/appengine ~/appenginecontent_copy
```

### Configure the Quiz application

1. Change to the directory that contains the sample files for this lab.

```bash
cd ~/appengine/startcontent_copy
```

1. Run the executable file by by running the following command:

```bash
. prepare_environment.shcontent_copy
```

Note: Please ignore warning and re-run the command if you are getting OperationError. Proceed with the lab ahead as some resources already exists.

This script file

- Creates an App Engine application.
- Creates a Cloud Storage bucket.
- Exports environment variables GCLOUD_PROJECT and GCLOUD_BUCKET.
- Creates a `virtualenv` isolated Python environment for Python 3 and activates it.
- Updates pip and runs `pip install -r requirements.txt`.
- Creates entities in Datastore.
- Creates IAM role and service accounts.
- Creates a Pub/Sub topic.
- Creates a Spanner Instance, Database, and Table.
- Creates a Cloud Function.
- Prints out the Project ID.

### Review the code

In this lab you'll view and edit files. You can use the shell editors that are installed on Cloud Shell, such as nano or vim, or use gcloud, the integrated code editor.

In this section, you use the Cloud Shell text editor to review the Quiz application code.

#### Launch the Cloud Shell Editor

From **Cloud Shell**, click **Open Editor**.

![open-editor.png](https://cdn.qwiklabs.com/eKqJzpi2fVYr10xqnfwERuNUT0q8sFk9vVJ813ujyPg%3D)

The integrated code editor launches in a separate tab of your browser, along with Cloud Shell.

#### Examine the code

Navigate to `/appengine/start`.

The folder structure for the Quiz application reflects how the application will be deployed in App Engine. For example, the web application is in the `frontend` folder, so you'll see the configuration files for App Engine; `app.yaml` file is present in the `frontend` folder.

## Prepare Application Code for App Engine Flexible Environment Deployment

### Create the app.yaml file for the frontend

In this section, you modify the configuration files for deployment of the Quiz application `frontend` into App Engine flexible environment.

1. In the Cloud Shell code editor, open `frontend/app.yaml`.

2. Add two key: value pairs:

   ```yaml
   runtime: python
   env: flexcontent_copy
   ```

   The first indicates that you want to use the python runtime. The second indicates that you want to use the flexible environment.

3. Add a third configuration entry, entrypoint to the `app.yaml` file.

   ```yaml
   entrypoint: "gunicorn -b 0.0.0.0:8080 quiz:app"content_copy
   ```

This value is the command-line that executes the Flask application, using the gunicorn HTTP server.

1. Add runtime_config.

```yaml
runtime_config:
  python_version: 3content_copy
```

1. Add a final configuration entry, `env_variables` to the `app.yaml` file. Include a key `GCLOUD_BUCKET` and the value from the `-media bucket` in your project. Be sure to replace `[GCLOUD_PROJECT]` with the **GCP Project ID** found in the left panel of the lab.

```yaml
env_variables:
  GCLOUD_BUCKET: "[GCLOUD_PROJECT]-media"content_copy
```

1. Save the file.

**app.yaml**

```yaml
runtime: python
env: flex
entrypoint: gunicorn -b 0.0.0.0:8080 quiz:app
runtime_config:
  python_version: 3  
env_variables:
  GCLOUD_BUCKET: "[GCLOUD_PROJECT]-media"content_copy
```

### Deploy the frontend to App Engine flexible environment

1. In Cloud Shell, setting the request timeout for the cloud build.

```bash
gcloud config set app/cloud_build_timeout 1800content_copy
```

1. Deploy the quiz application to App Engine flexible environment.

   ```bash
   gcloud app deploy ./frontend/app.yamlcontent_copy
   ```

   Enter **Y** to confirm.

   App Engine automatically packages, containerizes, and deploys the code.

   It will take up to 20 minutes to complete the deployment.

2. In the Cloud Platform Console, click **Navigation menu** > **App Engine**.

3. Click the link to your application in the top-right corner of the App Engine Dashboard.

   The link will be in the form `https://<PROJECT_ID>.appspot.com`.

   You should see your application.

   Click **Check my progress** to verify the objective.

Deploy the frontend to App Engine flexible environment.

Check my progress

## Update an App Engine Flexible Environment Application

In this section you modify the application code and then redeploy the application.

### Update the quiz application

1. In the Cloud Shell code editor, open `frontend/quiz/webapp/templates/home.html`.
2. Add several exclamation points to the top-level heading.

```html
{% extends 'master.html' %}
{% block head %}

    <title>Quiz - Python</title>
{% endblock %}

{% block content %}
    <h1>Welcome to the Quite Interesting Quiz!!!!!!</h1>
    <div class="jumbotron">
    <p>Welcome to the Quite Interesting Quiz where you can create a question, take a test or review feedback</p>
    </div>
    <h3 class="col-md-4"> <a href="/questions/add">Create Question</a></h3>
    <h3 class="col-md-4"> <a href="/client/">Take Test</a></h3>
    <h3 class="col-md-4"><a href="/leaderboard">Leaderboard</a></h3>
{% endblock %}content_copy
```

This small change stands in for all the changes you might make when updating an application.

1. Save the file.

### Deploy the updated application

1. In Cloud Shell, redeploy the App Engine application.

   ```bash
   gcloud app deploy ./frontend/app.yaml --no-promote \
   --no-stop-previous-versioncontent_copy
   ```

   Enter **Y** to confirm.

   Notice the two additional flags in the command, which mean that the previous version will continue to receive traffic.

   It may take up to 20 minutes to complete the updates to the deployment.

   Click **Check my progress** to verify the objective.

   Deploy the updated application.

   Check my progress

   In the Console, click **Navigation menu** > **App Engine** > **Dashboard**.

2. Click on the application URL in the top-right corner of the window. You should see that your application still displays the old title.

3. In the **App Engine** window, click **Versions**. You may need to refresh the page until you see two versions of the applications.

   ![ce3f927d2d2ab6ed.png](https://cdn.qwiklabs.com/oCp99eYo%2F%2Folz7EZGvRnyyyxrZ9WgbtQt72BgrvVFiU%3D)

   The version ID is in the form `yyyymmddthhmmss`, so it's easy to see which is the new and which is the old version.

   Click on both version links to see the new and old version of the quiz application.

   New version (notice those exclamation points!):

   ![5d4414237d6a2193.png](https://cdn.qwiklabs.com/uyeiDue7bpd7JTeUVfqSYn0%2BADyoCftV2DvD40SdXyE%3D)

   Old version:

   ![737770c3d5014496.png](https://cdn.qwiklabs.com/pzstOM09R4OO2uIn4%2FsfejnRlRLYRXvx850%2Bnwm%2BjFE%3D)

4. Enable the checkbox for both versions of the application, and click **Split traffic**.

   ![5e05eafcdad27aec.png](https://cdn.qwiklabs.com/NxqUAUSY4QqNqnpnOXXUMJj%2BjgJBd0AcrFonLIZUTpo%3D)

   Select the radio button to deliver versions randomly.

   Configure the traffic split to deliver 50% of traffic to the old version, and 50% to the new version.

   ![b479ab269633c349.png](https://cdn.qwiklabs.com/%2FIvi9WvEBLo2Rn6T1Car%2B14960ixk07ERmn1YwB9Ui0%3D)

   Click **Save**.

5. Return to the **Dashboard**, set **Version** to `All versions` and click the application link.

   ![d197378ef07b5623.png](https://cdn.qwiklabs.com/WPvNATelX7vy5apkG%2BeUNEkiyj%2FzVJ3AhHbfvhN%2FUuk%3D)

   You should see one version of the application.

6. Refresh the homepage a few times.

You should see that the homepage displays the old version approximately half the time, and the new version half the time.

In real-world scenarios, you might start by delivering small amounts of traffic to the new version in a canary release, and would use either a cookie or IP address to ensure that a client viewed a single consistent version of the application.

## End your lab

When you have completed your lab, click **End Lab**. Qwiklabs removes the resources you’ve used and cleans the account for you.

You will be given an opportunity to rate the lab experience. Select the applicable number of stars, type a comment, and then click **Submit**.

The number of stars indicates the following:

- 1 star = Very dissatisfied
- 2 stars = Dissatisfied
- 3 stars = Neutral
- 4 stars = Satisfied
- 5 stars = Very satisfied

You can close the dialog box if you don't want to provide feedback.